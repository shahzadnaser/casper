from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Forms"),
			"items": [
				{
					"type": "doctype",
					"name": "Transaction",
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Device",
				},
            ]
		}
    ]